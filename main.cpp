#include <iostream>
#include <vector>
#include <list>
#include <unordered_set>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <math.h>
#include <map>
using namespace std;

unordered_set<string> words;
map<string,unordered_set<string> > synonyms;

int sumVector(vector<int> x){
    int sum = 0;
    for(int i : x)
        sum += i;
    return sum; 
}

vector<int> convert(int x, int length){
    vector<int> res;
    while(x){
        if(x&1)
            res.push_back(1);
        else
            res.push_back(0);
        x>>=1;
    }
    if((int)res.size() < length)
        while((int)res.size() < length)
            res.push_back(0);

    return res;
}

list<vector<int> > createBitVector(int length){
    list<vector<int> > res;
    vector<int> nul(length,0);
    res.push_back(nul);
    int i = 1;
    while(sumVector(res.back()) < length){

        vector<int> curBitVec = convert(i,length);
        if(sumVector(curBitVec) > 1)
            res.push_back(curBitVec);

        i++;
    }
    res.pop_front(); //delete null

    return res;
}

list<string> split(string in, char del){
    stringstream  ss(in);
    string item;
    list<string> l;
    while(getline(ss,item,del)){
        l.push_back(item);
    }
    return l;
}

struct scoreHit{
	int score;
	string name;
	string formattedLine;
	vector<int> bitVec;
	string desc;
	scoreHit(int s,string n,vector<int> b,string l,string d){
		score=s;
		name=n;
		bitVec=b;
		formattedLine=l;
		desc=d;
	}
};

bool pairCompare(const scoreHit& firstElem, const scoreHit& secondElem) {
  return firstElem.score < secondElem.score;
}

string listToString(list<string> l){
    string res = "";
    for(string x : l)
        res += x;
    return res;
}

int main(int argc, char *argv[])
{

    if(argc <= 2){

        cout << "-------------------------"<<endl;
        cout << "USAGE : \n" << endl;
        cout << " ./pen <PATH TO DICTONARY> \"some sentence that describes something\"" << endl;
        cout << " <PATH TO DICTONARY> = A dictonary csv file containing words linewise. The dictonary can additionally have synonyms separated by \",\"." << endl;
        cout << " every word in the sentence is separeted by \" \"." << endl;
        cout << " Use the form \"word1:word2:...\" to indicate different possible words for the same position." << endl;
        cout << " Output : possible names for the methode/thing" << endl;
        cout << " Example ./pen \"programm that systematically analyses:evaluates descriptions:texts for finding names\""<< endl;
        cout << " -> pen = (P)rogramm that systematically analyses:(E)valuates descriptions:texts for finding (N)ames"<< endl;
        cout << "-------------------------"<<endl;

        return -1;
    }

    string line;
    string pathToDictionary = argv[1];
    unsigned int numSyn = 0;
    ifstream myfile;

    myfile.open(pathToDictionary);
    if (myfile.is_open()){
        while ( getline (myfile,line) ){
        	string::size_type pos = line.find_last_not_of("\n\r\t");
			if(pos != string::npos)
				line = line.substr(0, pos+1);

        	if(line=="" || line=="\n")continue;

    		transform(line.begin(), line.end(), line.begin(), ::tolower);
        	
        	list<string> line_split=split(line,',');
        	
        	if(line_split.size()==0 || line_split.front().length()==0 || split(line_split.front(),' ').size()>1)continue;
        	
        	string cur_word = line_split.front();
            words.insert(cur_word);
            line_split.pop_front();

            for(string s : line_split){
    			transform(s.begin(), s.end(), s.begin(), ::tolower);
    			pos = s.find_last_not_of(" ");
				if(pos != string::npos)
					s = s.substr(0, pos+1);

        		if(s.length()==0 || split(s,' ').size()>1 || strcmp(s.c_str(),cur_word.c_str())==0)continue;
            	numSyn++;
            	synonyms[cur_word].insert(s);
            }
        }
        myfile.close();
    }else{
        cout << "ERROR : could not load "+pathToDictionary << endl;
        return -1;
    }

    if(words.size()<1){
        cout << "ERROR : loading the dictonary "+pathToDictionary+" resulted in 0 words." << endl;
        return -1;
    }
    cout << "Loading Dictionary : DONE " << endl;
    cout << "#words : " << words.size() << endl;
    cout << "#synonyms : " << numSyn << endl;

    vector<scoreHit> results;

    for(int i = 2 ; i < argc ; i++){

        string curMaster = argv[i];
        transform(curMaster.begin(), curMaster.end(), curMaster.begin(), ::tolower);

        list<string> splitMaster = split(curMaster,' ');
        vector<string> splitMasterVec(splitMaster.begin(),splitMaster.end()); //current sentence as vector of string

        list<vector<int> > bitVecList = createBitVector(splitMaster.size()); //create all possible bitvectors of size 2 or greater = all possible subsentences with at least 2 words.

        unordered_set<string> curMaster_set;
        for(string cur_word : splitMaster){ // evaluate each word of the subsentence
        	list<string> w_list=split(cur_word,':');
        	for(string cur_word2 : w_list){ // evaluate each word of the subsentence
        		curMaster_set.insert(cur_word2);
        	}
    	}

		int cc = 0;

        for(vector<int> curBitVec : bitVecList){ //evaluate the current bitvector / the current selection of words / the current subsentece

            if( cc % bitVecList.size()>100 ? (bitVecList.size()/100) : 1 == 0)
                cout << round(100*(double)cc/(double)bitVecList.size()) << " %" << '\r' << flush;
            cc++;

            string desc="";

            list<string> cur_splitMasterVec; //current subsentence
            int score = 1;

            int sumV = sumVector(curBitVec); // number of words in the subsentence

            if(curBitVec.front() && curBitVec.back()){ //BONUS only first and last word matches
                score += splitMaster.size();
                desc +="+"+to_string(splitMaster.size())+":first and last word matches. ";
            }

            score += sumV*2; //add the number of used words to the score

            for(int j = 0 ; j < curBitVec.size() ; j++)
                if(curBitVec[j])
                    cur_splitMasterVec.push_back(splitMasterVec[j]); //create the corresponding subsentence 

            list<pair<list<string>,string> > ids;
            list<string> nul;
            nul.push_back("");
            ids.push_back(make_pair(nul,""));

            for(unsigned int id = 0 ; id < splitMasterVec.size() ; id++){ // string cur_word : cur_splitMasterVec){ // evaluate each word of the subsentence

            	if(curBitVec[id]){

            		list<pair<list<string>,string> > cur_ids = ids;
	                ids.clear();

	                for(string possible_word : split(splitMasterVec[id],':')){ // loop over all possible equivalent words
	                    for(pair<list<string>,string> cur_id : cur_ids){
	                        int maxK;
	                        possible_word.length() <= 5 ? ( possible_word.length() <= 3 ? maxK = 2 : maxK = 3 ) : maxK = 4; //evaluate up to 4 letters of the current word (if the word is too small, then less than 4)
	                        for(int k = 1 ; k < maxK ; k++){
	                            pair<list<string>,string> cur_id2 = cur_id;
	                            cur_id2.first.push_back(possible_word.substr(0,k)); 
	                            string fat = possible_word.substr(0,k);
			                    transform(fat.begin(), fat.end(), fat.begin(), ::toupper);
	                            cur_id2.second+="\33[4m\033[1;31m"+fat+"\033[0m\33[0m"+possible_word.substr(k)+" ";

	                            ids.push_back(cur_id2); //add to the current name (=id)

	                        }
	                    }
	                }
            	}else{
            		for(list<pair<list<string>,string> >::iterator it = ids.begin(); it != ids.end(); ++it){
            			(*it).second+=splitMasterVec[id]+" ";
            		}
            	}
                
            }

            for(pair<list<string>,string> cur_id : ids){ //for each id (=name) add to the results 
                string cur_id_str = listToString(cur_id.first);
                if(cur_id_str.size() > 0 && words.count(cur_id_str) ){
                	if(curMaster_set.count(cur_id_str)){
		                score += splitMaster.size();
		                desc +="+"+to_string(splitMaster.size())+":direct match";
	            	}
					for(string s : synonyms[cur_id_str])
						if(curMaster_set.count(s)){
						    score += splitMaster.size();
						    desc +="+"+to_string(splitMaster.size())+":synonym of "+s+".";
						}


                    results.push_back( scoreHit( 
                    					score - splitMaster.size()/cur_id_str.length() , //score
                    					cur_id_str , //name
                    					curBitVec , //bitscore => subsentence used in name
                    					cur_id.second,
                    					desc ) ); //description (Bonus ...)
                }
            }
        }

        //free memory
        synonyms.clear();
        words.clear();

        cout << "number of results found : "<<results.size() << endl;

        sort(results.begin() , results.end() , pairCompare);

        cout << "RESULTS :" << endl;
        cout << "score | name | sentence | description" << endl;

        for(scoreHit i : results){

            cout << i.score << " | " << i.name << " | " << i.formattedLine;

			int crit_i = 0;
			int idx=0;

      //       for(string s : splitMaster){ //go over the input sentence vector
      //       	if(i.bitVec[idx]){ //if the current word is used in the name -> mark the letters
	     //            for(string possible_word : split(s,':')){ //go over all possible equivalent words in this position
	     //            	bool foundOne=false;
						// int maxK;
						// //find the substring of the word corresponding to the part in the name 
			   //          possible_word.length() <= 5 ? ( possible_word.length() <= 3 ? maxK = 2 : maxK = 3 ) : maxK = 4;
			   //          for(int k = 1 ; k < maxK ; k++){
			   //              if(i.name.substr(crit_i,k) == possible_word.substr(0,k)){
			   //                  string fat = possible_word.substr(0,k);
			   //                  string small = possible_word.substr(k);
			   //                  transform(fat.begin(), fat.end(), fat.begin(), ::toupper);
			   //              	cout <<"\33[4m" << "\033[1;31m" << fat <<"\033[0m" << "\33[0m" << small << " "; //mark it red
			   //              	crit_i+=k;
			   //              	foundOne=true;
			   //              }
			   //          }
			   //          if(foundOne)break;
			   //      }

      //       	}else
      //       	{// the current word is unused in the name -> plain output
      //       		cout << s << " ";
      //       	}
      //       	idx++;
      //       }
            cout << "| "<<i.desc << endl;
        }
    }
}
