(p)rogramm for a systematic analysis and (e)valuation of descriptions and texts for finding (n)ames. -> pen


**COMPILE :**


g++ -std=c++11 main.cpp -o pen


**USAGE :**


 ./pen "some sentence that describe a methode or a thing"
 
 every word in the sentence has to be separeted by " ".
 
 Use the form "word1,word2,..." to indicate different possible words for the same word-position.
 
 Output : possible acronyms for the text found in a given dictonary
 
 Example ./pen "programm for a systematic analysis and evaluation of descriptions and texts for finding names"
 
 output 
 
    pen = (p)rogramm for a systematic analysis and (e)valuation of descriptions and texts for finding (n)ames
    

